import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Row,Col} from 'react-bootstrap';
import Calculator from './HW11/Calculator';

function App() {
  return (
    <Calculator/>
  );
}

export default App;

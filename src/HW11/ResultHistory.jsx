import React, { Component } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import { ListGroup } from "react-bootstrap";
import './calculatorstyle.css'

export default class ResultHistory extends Component {
    render() {
        return (
                  <ListGroup.Item className="list-item">{this.props.data}</ListGroup.Item>
        )
    }
}

import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import pic1 from './calculator.png'
import './calculatorstyle.css'
import ResultHistory from './ResultHistory'
import {Container,Row,Card,Button, ListGroup} from 'react-bootstrap';

export default class Calculator extends Component {
    constructor(){
        super();
        this.state={
            historys: []
        };}

    Calculate(){
        var pattern = /[^0-9]/gi;
        var num1=this.num1.value;
        var num2=this.num2.value;
        var operation=this.operation.value;
        var result,history;
        if(num1.match(pattern)!=null||num1===""||num2.match(pattern)!=null||num2===""){
            alert("Invald Input or Input missing! Please Check!")
        }else{
            if (operation==="+") result=Number(num1)+Number(num2);
            else if (operation==="-") result=num1-num2;
            else if (operation==="*") result=num1*num2;
            else if (operation==="/") result=num1/num2;
            else result=num1%num2;
            history=num1+ " " + operation + " " + num2 + " = " + result;
            this.setState({
                historys:this.state.historys.concat(history)
            })
            
        }
    }

    render() {

        var listitem = this.state.historys.map((dataArg,index) => (
            <ResultHistory data={dataArg} key={index}/>
        ));

        return (
            <Container>
                <Row>
                    <Card style={{ width: '18rem' }}>
                        <Card.Img variant="top" src={pic1} />
                        <Card.Body>
                            <input 
                                type="text"
                                name="num1"
                                defaultValue=""
                                ref={(num1)=> (this.num1=num1)}
                            />
                            <input 
                                type="text"
                                name="num2"
                                defaultValue=""
                                ref={(num2)=> (this.num2=num2)}
                            />
                            <select 
                                id="sign"
                                ref={(operation)=>(this.operation=operation)}
                            >
                                <option value="+">(+) Plus</option>
                                <option value="-">(-) Subtract</option>
                                <option value="*">(*) Multiply</option>
                                <option value="/">(/) Divide</option>
                                <option value="%">(%) Module</option>
                            </select>
                            <Button  
                                id="btnCal"
                                onClick={this.Calculate.bind(this)}>
                                    Calculate
                            </Button>
                        </Card.Body>
                    </Card>
                    <div>
                    <h2>Result History</h2>
                    <ListGroup>{listitem}</ListGroup>
                    </div>
                </Row>
            </Container>
        )
    }
}
